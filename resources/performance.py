import time
import socket
import matplotlib.pyplot as plt

HOST = 'localhost'
PORT = 12355

requests = []
for i in range(200):
    requests.append(i)

request_times = []
start = time.time()
for _ in range(200):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.send('1'.encode())
        time.sleep(0.001)
        s.send('286.txt'.encode())
        time.sleep(0.005)
        s.send('12384'.encode())
        time.sleep(0.001)
    request_times.append((time.time() - start) - 0.006)

plt.plot(requests, request_times)   
plt.xlabel('Number of requests') 
plt.ylabel('Request times') 
plt.title('Requests by 1 client') 
plt.show() 