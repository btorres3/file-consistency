import os
import sys
import random
import string
from signal import signal, SIGPIPE, SIG_DFL

# Thank you to PyNative for randomString()
# https://pynative.com/python-generate-random-string/
def randomString(stringLength):
    """Generate a random string with the combination of lowercase and uppercase letters """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))
    
signal(SIGPIPE, SIG_DFL)

file = sys.argv[1]
file_num, file_extension = os.path.splitext(file)

try:
    num = int(file_num)
except ValueError:
    print("Please enter in a valid file name!")
    sys.exit(0)

n_id = 0
n_counter = 10
for i in range(1, 301):
    if num == i:
        n_id = n_counter
    if i % 10 == 0:
        n_counter += 1

s_id = 0
for i in range(10, 40):
    if n_id == i:
        break
    if i % 3 == 0:
        s_id += 1

print(f"Editing {file} at super-peer {s_id} and leaf-node {n_id}...")
path = "./resources/" + "SP" + str(s_id) + "/LN" + str(n_id) + "/Files/" + file

with open(path, "a") as f:
    f.write(randomString(10))