import os
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL)
n_id = 10
sp_id = 0
files_to_delete = f"./SP{sp_id}/LN{n_id}"
place_to_save = f"./SP{sp_id}/LN{n_id}/Files"
downloads = f"./SP{sp_id}/LN{n_id}/Downloads"

counter = 1
sp_counter = 1
default_size = 1024

for x in range(1, 301):
    tot = counter * default_size
    cmd1 = f'rm {files_to_delete}/* >/dev/null 2>&1'
    cmd2 = f'mkdir {place_to_save} >/dev/null 2>&1'
    cmd3 = f'mkdir {downloads} >/dev/null 2>&1'
    cmd4 = f'base64 /dev/urandom | head -c {tot} > {place_to_save}/{x}.txt'
    os.system(cmd1)
    os.system(cmd2)
    os.system(cmd3)
    os.system(cmd4)
    counter += 1
    sp_counter += 1
    if counter == 11:
        counter = 1
        n_id += 1
        files_to_delete = f"./SP{sp_id}/LN{n_id}"
        place_to_save = f"./SP{sp_id}/LN{n_id}/Files"
        downloads = f"./SP{sp_id}/LN{n_id}/Downloads"
    if (sp_counter - 1) % 30 == 0:
        sp_id += 1
        files_to_delete = f"./SP{sp_id}/LN{n_id}"
        place_to_save = f"./SP{sp_id}/LN{n_id}/Files"
        downloads = f"./SP{sp_id}/LN{n_id}/Downloads"
