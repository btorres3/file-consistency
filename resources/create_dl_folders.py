import os
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL)
sp_id = 0

for i in range(10, 40):
    folder = f"./resources/SP{sp_id}/LN{i}/Downloads"
    cmd = f'mkdir {folder} >/dev/null 2>&1'
    os.system(cmd)
    if i % 3 == 0:
        sp_id += 1