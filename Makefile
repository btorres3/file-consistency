PYTHON   := python3.7
PIP      := python3-pip
TMUX     := tmux
TELNET   := telnet
SRC      := ./file-consistency
ALT_SRC  := ./resources
NODE     := ${SRC}/node.py
SP       := ${SRC}/super_peer.py
TOPOLOGY := a
CONSIS   := push
CREATE   := ${ALT_SRC}/create_dl_folders.py
EDITOR   := ${ALT_SRC}/file_editor.py
FILE     := 1.txt

default:
	@$(PYTHON) $(CREATE) &
	@$(PYTHON) $(SP) 0 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 1 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 2 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 3 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 4 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 5 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 6 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 7 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 8 $(TOPOLOGY) $(CONSIS) &
	@$(PYTHON) $(SP) 9 $(TOPOLOGY) $(CONSIS) &

	@$(PYTHON) $(NODE) 10 0 $(CONSIS) &
	@$(PYTHON) $(NODE) 11 0 $(CONSIS) &
	@$(PYTHON) $(NODE) 12 0 $(CONSIS) &
	@$(PYTHON) $(NODE) 13 1 $(CONSIS) &
	@$(PYTHON) $(NODE) 14 1 $(CONSIS) &
	@$(PYTHON) $(NODE) 15 1 $(CONSIS) &
	@$(PYTHON) $(NODE) 16 2 $(CONSIS) &
	@$(PYTHON) $(NODE) 17 2 $(CONSIS) &
	@$(PYTHON) $(NODE) 18 2 $(CONSIS) &
	@$(PYTHON) $(NODE) 19 3 $(CONSIS) &
	@$(PYTHON) $(NODE) 20 3 $(CONSIS) &
	@$(PYTHON) $(NODE) 21 3 $(CONSIS) &
	@$(PYTHON) $(NODE) 22 4 $(CONSIS) &
	@$(PYTHON) $(NODE) 23 4 $(CONSIS) &
	@$(PYTHON) $(NODE) 24 4 $(CONSIS) &
	@$(PYTHON) $(NODE) 25 5 $(CONSIS) &
	@$(PYTHON) $(NODE) 26 5 $(CONSIS) &
	@$(PYTHON) $(NODE) 27 5 $(CONSIS) &
	@$(PYTHON) $(NODE) 28 6 $(CONSIS) &
	@$(PYTHON) $(NODE) 29 6 $(CONSIS) &
	@$(PYTHON) $(NODE) 30 6 $(CONSIS) &
	@$(PYTHON) $(NODE) 31 7 $(CONSIS) &
	@$(PYTHON) $(NODE) 32 7 $(CONSIS) &
	@$(PYTHON) $(NODE) 33 7 $(CONSIS) &
	@$(PYTHON) $(NODE) 34 8 $(CONSIS) &
	@$(PYTHON) $(NODE) 35 8 $(CONSIS) &
	@$(PYTHON) $(NODE) 36 8 $(CONSIS) &
	@$(PYTHON) $(NODE) 37 9 $(CONSIS) &
	@$(PYTHON) $(NODE) 38 9 $(CONSIS) &
	@$(PYTHON) $(NODE) 39 9 $(CONSIS) &

kill:
	@pkill -f "file-consistency"

prepare-dev:
	@sudo dnf install $(TMUX) $(TELNET)

edit:
	@$(PYTHON) $(EDITOR) $(FILE)
