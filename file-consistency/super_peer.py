import os
import sys
import json
import time
import socket
import threading
import xmlrpc.client
from xmlrpc.client import ServerProxy
from socketserver import ThreadingMixIn
from xmlrpc.server import SimpleXMLRPCServer

class SimpleThreadedXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
    """Adds multithreading to Python's SimpleXMLRPCServer
    """
    pass

class SuperPeer(object):
    """Creates a super-peer for the network

    Keyword arguments:
    id        -- its unique ID
    nodes     -- list of leaf-nodes its responsible for
    neighbors -- list of its super-peer neighbors
    """
    def __init__(self, id, nodes, neighbors, consistency):
        self.id = id
        self.nodes = nodes
        self.neighbors = neighbors
        self.consistency = consistency

        self.files = {}
        self.dl_files = {}
        if consistency == "pull_2":
            self.mod_files = {}
            # Assign one thread to check ttr of its modified files
            threading.Thread(target=self._check_ttr,
                            args=(), daemon=False).start()
        self.msgs = [[] for _ in range(50)]
        self.folder = "./resources/SP" + str(id)

        # Assign one thread to clear its msgs list every 30 seconds
        threading.Thread(target=self._update,
                         args=(), daemon=False).start()
        # Assign one thread to run the RPC server
        threading.Thread(target=self._run_rpc_server,
                         args=(), daemon=False).start()

    def _run_rpc_server(self):
        """Helper method that registers this class' functions and starts the RPC server"""
        sp_server = SimpleThreadedXMLRPCServer(
            ('', 9090 + self.id), allow_none=True, logRequests=False)
        sp_server.register_instance(self)
        sp_server.serve_forever()

    def search(self, msg_id, ttl, file_name):
        """RPC method that starts query() and uses topology info"""
        arr = []
        if self.id not in msg_id[1]:
            msg_id[1].append(self.id)
        
        if len(self.neighbors) > 1:
            arr.append(self.query(msg_id, ttl, file_name))

        for neighbor in self.neighbors:
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            arr.append(proxy.query(msg_id, ttl, file_name))
        
        return arr

    def query(self, msg_id, ttl, file_name):
        """Asks a super peer to see if it has file_name"""
        if ttl == 0:
            return None   
        ttl -= 1

        leaf_list = self.files.get(file_name, [])
        # vers_check = self.dl_files.get(file_name, [])

        msg_id_list = [msg_id[0], msg_id[2]]
        if msg_id_list not in self.msgs:
            self.msgs.append(msg_id_list)
        else:
            print(f'Super-peer #{self.id}: I\'ve already seen this!')
            return None

        if msg_id[0] in self.nodes and leaf_list:
            return self.query_hit(msg_id, 10, file_name, 'localhost', 12345 + leaf_list[0])
        elif leaf_list and msg_id[0] not in self.nodes:
            neighbor = msg_id[1].pop()
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            ip = 'localhost'
            port = 12345 + leaf_list[0]
            return proxy.query_hit(msg_id, 10, file_name, ip, port)
        elif len(self.neighbors) == 1:
            neighbor = self.neighbors[0]
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            msg_id[1].append(self.id)
            return proxy.query(msg_id, ttl, file_name)
        else:
            return None

    def query_hit(self, msg_id, ttl, file_name, leaf_node_ip, leaf_port):
        """A super peer has file_name and will send back leaf_port in reverse order the request came"""
        if ttl == 0:
            return None   
        ttl -= 1
        if msg_id[0] not in self.nodes:
            neighbor = msg_id[1].pop()
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            return proxy.query_hit(msg_id, ttl, file_name, leaf_node_ip, leaf_port)
        elif msg_id[0] in self.nodes:
            return leaf_port

    def registry(self, node_id, node_files):
        """An RPC method that allows peers to register their files with the server."""
        for f in node_files:
            try:
                if node_id not in self.files[f]:
                    self.files[f].append(node_id)
            except KeyError:
                self.files[f] = [node_id]

        print(f"Leaf Node {node_id} has successfully registered all its Files.")

    def registry_dl(self, node_id, dl_files, version):
        """An RPC method that allows peers to register their downloaded files with the server."""
        for f in dl_files:
            try:
                if node_id not in self.files[f]:
                    self.dl_files[f].append(node_id)
                    self.dl_files[f].append(version)
            except KeyError:
                self.dl_files[f] = [node_id, version]

        print(
            f"Leaf Node {node_id} has successfully registered its downloaded Files.")

    # Push-approach methods

    def invalidate(self, msg_id, origin_id, file_name, version_num):
        """An RPC method where this superpeer will send an invalidation message to its neighbors."""
        self.invalidation(msg_id, origin_id, file_name, version_num)
        for neighbor in self.neighbors:
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            proxy.invalidation(msg_id, origin_id, file_name, version_num)

    def invalidation(self, msg_id, origin_id, file_name, version_num):
        """An RPC method that tells all of this superpeer's nodes to check version_num of file_name"""
        for node in self.nodes:
            port_num = node + 9090
            proxy = ServerProxy(f'http://localhost:{port_num}')
            proxy.check(file_name, version_num)

    # Pull-approach 2 methods
    def new_version(self, file_name, version_num, ttr):
        """An RPC method that stores an array of modified files of its nodes"""
        self.mod_files[file_name] = [version_num, ttr]

    def query_version(self, file_name, new_version):
        """An RPC method that tells a node to delete its out-of-date file"""
        values = self.dl_files.get(file_name, None)
        if values:
            node = values[0]
            version = values[1]
        else:
            return None
        if version == new_version:
            return
        else:
            self.dl_files.pop(file_name, None)
            node_rpc_port = 9090 + node
            proxy = ServerProxy(f'http://localhost:{node_rpc_port}')
            proxy.delete_ood_file(file_name)
    
    def _check_ttr(self):
        """Helper method that checks the ttr of all its modified files"""
        while True:
            # key = file
            # values = [version, ttr]
            for key, values in self.mod_files.items():
                time.sleep(15)
                time.sleep(values[1])
                # The ttr has expired
                for neighbor in self.neighbors:
                    proxy = ServerProxy(f'http://localhost:909{neighbor}')
                    proxy.query_version(key, values[0])

    def _update(self):
        while True:
            time.sleep(30)
            self.msgs.clear()

# To start a super peer, it requires three command line arguments:
# Argument 1: the id of the peer itself
# Argument 2: the topology (a for all-to-all, l for linear)
# To get its neighbors and peers, I have a created a JSON file
# that statically allocates all this information, so its neighbors/peers
# are determined by its user-provided id and user-provided topology
try:
    sp_id = int(sys.argv[1])
    with open(r"./resources/nodes.json") as file:
        nodes_dict = json.load(file)
    info = nodes_dict.get(sys.argv[1])
    nodes = [int(i) for i in info.get('leaf_nodes')]
    if sys.argv[2] == "l":
        neighbors = [int(i) for i in info.get('linear')]
    elif sys.argv[2] == "a":
        neighbors = [int(i) for i in info.get('all_to_all')]
    consistency = sys.argv[3]
    sp = SuperPeer(sp_id, nodes, neighbors, consistency)
except ValueError:
    print("Please enter in a valid integer!")
    sys.exit(0)
