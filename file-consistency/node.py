import os
import sys
import json
import time
import socket
import threading
import xmlrpc.client
from menu import Menu
from xmlrpc.client import ServerProxy
from xmlrpc.server import SimpleXMLRPCServer

with open(r"./resources/nodes.json") as file:
    nodes_dict = json.load(file)
ttr = int(nodes_dict.get('ttr').get('value'))

# Index in array for various file attributes
VERSION = 0
ORIGIN  = 1
TTR_IND = 2
LMT     = 3
ATTR = [VERSION, ORIGIN, TTR_IND, LMT]

class Node:
    """Creates a leaf-node for the network

    Keyword arguments:
    node_id       -- its unique ID
    super_peer_id -- the ID of its super-peer
    """
    def __init__(self, node_id, super_peer_id, consistency):
        self.node_id = node_id
        self.super_peer_id = super_peer_id
        self.consistency = consistency
        self.times = []

        self._create_file_list()

        self.proxy = ServerProxy(f'http://localhost:909{super_peer_id}', allow_none=True)
        self.proxy.registry(self.node_id, list(self.files))

        # Assign one thread to run the RPC server
        threading.Thread(target=self._run_rpc_server, args=(), daemon=True).start()
        # Assign one thread to check for modifications to its master files
        threading.Thread(
            target=self._check_for_file_modifications, daemon=True).start()
        self._consistency_start()
        # The main thread will run the server-side of the peer
        self._run_server()
        
    def _create_file_list(self):
        """Helper method that creates the node's file array and folder variable"""
        self.folder = "./resources/" + "SP" + str(self.super_peer_id) + "/LN" + str(self.node_id) + "/Files"
        self.downloads = "./resources/" + "SP" + \
            str(self.super_peer_id) + "/LN" + str(self.node_id) + "/Downloads"
        self.files = {i : [1, self.node_id, ttr, 1] for i in os.listdir(self.folder)}
        self.dl_files = {i : [1, self.node_id, ttr, 1] for i in os.listdir(self.downloads)}
        for file in list(self.files):
            path = os.path.join(self.folder, file)
            lmt = os.path.getmtime(path)
            self._set_attr(file, TTR_IND, ttr)
            self._set_attr(file, LMT, lmt)

    def _set_attr(self, file, attr, val):
        """Helper method that sets the requested attributes of file to val"""
        attributes = []
        if file in self.files.keys():
            attributes = self.files[file]
        elif file in self.dl_files.keys():
            attributes = self.dl_files[file]
        attributes[attr] = val

    def _get_attr(self, file, attr):
        """Helper method that gets the requested attributes of file"""
        attributes = []
        if file in self.files:
            attributes = self.files[file]
        elif file in self.dl_files:
            attributes = self.dl_files[file]
        return attributes[attr]

    def _run_rpc_server(self):
        """Helper method that registers this class' functions and starts the RPC server"""
        node_server = SimpleXMLRPCServer(
            ('', 9090 + self.node_id), allow_none=True, logRequests=False)
        node_server.register_instance(self)
        node_server.serve_forever()

    def _consistency_start(self):
        if self.consistency == "pull_1":
            # This thread will continously check the TTR of its downloaded files
            threading.Thread(target=self._check_ttr, daemon=True).start()
        else:
            # Assign one thread to run automatic updates
            threading.Thread(target=self._update, daemon=True).start()

    def obtain(self, file_name):
        """Helper method that returns a file to another leaf-node"""
        file_to_send = os.path.join(self.folder, file_name)
        with open(file_to_send, "rb") as handle:
            return xmlrpc.client.Binary(handle.read())
        
    def _run_server(self):
        """Run the server-side of the leaf node"""
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('', 12345 + self.node_id))
        server_socket.listen(5)

        while True:
            try:
                (client, _) = server_socket.accept()
                threading.Thread(target=self._client_handler,
                                 args=(client,)).start()
            except KeyboardInterrupt:
                print("Exiting...")
                sys.exit(0)

    def _client_handler(self, client):
        """Helper method that handles clients once they are connected"""
        menu = Menu(client, self.node_id, False, self.files.keys(), self.dl_files.keys())
        seq_num = 0
        while True:
            choice = menu.menu()
            if choice == 1:
                file_name = menu.search_for_file()
                port_arr = self.proxy.search([self.node_id, [self.super_peer_id], seq_num], 10, file_name)
                seq_num += 1
                port_num = menu.choose_port(port_arr)
                if not port_num:
                    client.send(b'\r\nFile not found!\r\n\r\n')
                    continue

                client.send(b'Downloading now...\r\n\r\n')

                node_rpc_port = (port_num - 12345) + 9090
                proxy2 = ServerProxy(
                    f'http://localhost:{node_rpc_port}')
                
                place_to_save = self.downloads + f"/{file_name}"

                with open(place_to_save, "wb") as handle:
                    handle.write(proxy2.obtain(file_name).data)

                if self.consistency == "pull_1":
                    self.dl_files.update({file_name: [1, self.node_id, ttr, 1]})

                while file_name not in self.dl_files:
                    if file_name in self.dl_files:
                        break

                values = proxy2.get_metadata(file_name)
                for i, val in enumerate(values):
                    self._set_attr(file_name, ATTR[i], val)

                my_version = self._get_attr(file_name, VERSION)
                self.proxy.registry_dl(self.node_id, list(self.dl_files), my_version)

            elif choice == 2:
                menu.display()
            elif choice == 3:
                file_to_remove = menu.remove()
                if file_to_remove == 'q':
                    continue
                os.remove(f"{self.downloads}/{file_to_remove}")
                if self.consistency != "push":
                    del self.dl_files[file_to_remove]

    def _update(self):
        """Helper method that updates each leaf nodes's directory every 1 second and prints deletions or additions"""
        while True:
            time.sleep(1)
            directory_to_track = os.listdir(self.downloads)
            less = [item for item in list(self.dl_files) if item not in directory_to_track]
            more = [item for item in directory_to_track if item not in list(self.dl_files)]
            if less:
                print(f"File(s) deleted for leaf node {self.node_id}!")
                for item in less:
                    print(f"{item} removed.")
                    del self.dl_files[item]

            elif more:
                print(f"File(s) added for leaf node {self.node_id}!")
                for item in more:
                    print(f"{item} added.")
                    self.dl_files.update({item: [1, self.node_id, ttr, 1]})

    def _check_for_file_modifications(self):
        """Helper method that checks each leaf nodes's files for modifications and updates the version number accordingly"""
        for file in self.files.keys():
            file_path = os.path.join(self.folder, file)
            self.times.append(os.path.getmtime(file_path))
        while True:
            time.sleep(1)
            for i, file in enumerate(self.files.keys()):
                file_path = os.path.join(self.folder, file)
                if os.path.getmtime(file_path) != self.times[i]:
                    new_version = self._get_attr(file, VERSION) + 1
                    self._set_attr(file, VERSION, new_version)
                    self.times[i] = os.path.getmtime(file_path)
                    if self.consistency == "push":
                        self.proxy.invalidate(
                            [self.super_peer_id], self.node_id, file, new_version)
                    elif self.consistency == "pull_2":
                        self.proxy.new_version(file, new_version, 15)

    # Get metadata for files from peers you download the file from

    def get_metadata(self, file_name):
        """RPC method that returns metadata of a file to the requesting leaf-node"""
        my_version = self._get_attr(f'{file_name}', VERSION)
        my_origin = self._get_attr(f'{file_name}', ORIGIN)
        my_ttr = self._get_attr(f'{file_name}', TTR_IND)
        my_lmt = self._get_attr(f'{file_name}', LMT)

        return [my_version, my_origin, my_ttr, my_lmt]

    # Push-approach methods

    def check(self, file_name, new_version):
        """RPC method that compares its version of file_name and compares it to new_version"""
        if file_name in self.dl_files.keys():
            file_path = os.path.join(self.downloads, file_name)
            my_version = self._get_attr(file_name, VERSION)
            if my_version == new_version:
                pass
            else:
                print(f'Invalid version of {file_name}! Deleting now.')
                os.remove(f"{file_path}")

    # Pull-approach 1 methods

    def compare_version(self, file_name, their_version):
        """RPC method that returns its comparison of my_version of file_name to their_version"""
        my_version = self._get_attr(f"{file_name}", VERSION)
        return my_version == their_version

    def _check_ttr(self):
        """Helper method that checks the ttr of all of this node's downloaded files"""
        while True:
            time.sleep(15)
            for f in list(self.dl_files):
                ttr = self._get_attr(f"{f}", TTR_IND)
                # The file could've been deleted between the start and end of the TTR
                if f not in list(self.dl_files):
                    continue
                time.sleep(ttr)
                # The ttr has expired
                my_version = self._get_attr(f"{f}", VERSION)
                origin_rpc = 9090 + self._get_attr(f"{f}", ORIGIN)
                proxy2 = ServerProxy(
                    f'http://localhost:{origin_rpc}')
                same = proxy2.compare_version(f, my_version)
                if same:
                    continue
                elif not same:
                    print(f"The version of {f} is not the same as the master copy! Deleting file...")
                    os.remove(f"{self.downloads}/{f}")
                    del self.dl_files[f]
                
    # Pull-approach 2 methods
    def delete_ood_file(self, file_name):
        """RPC method that deletes the out of date file_name"""
        print(f"Super-peer has said our version of {file_name} is not the same as the master copy! Deleting file...")
        os.remove(f"{self.downloads}/{file_name}")

# To start a leaf-node, it requires two command line arguments:
# Argument 1: the id of the node itself
# Argument 2: the id of its super peer
try:
    n_id = int(sys.argv[1])
    sp_id = int(sys.argv[2])
    consistency = sys.argv[3]
    node = Node(n_id, sp_id, consistency)
except ValueError:
    print("Please enter in a valid integer!")
    sys.exit(0)
