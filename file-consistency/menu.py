import socket

class Menu:
    def __init__(self, client, unique_id, is_super_node, files, downloads):
        """Creates a leaf-node for the network

        Keyword arguments:
        client        -- client socket
        unique_id     -- the ID of the node/super-peer using this instance
        is_super_node -- is this instance being used by a super-peer
        files         -- the files that belong to node/super-peer using this instance
        downloads     -- the downloaded files that belong to node/super-peer using this instance
        """
        self.client = client
        self.id = unique_id
        self.is_super_node = is_super_node
        self.files = files
        self.downloads = downloads

    def menu(self):
        """Helper method that displays user-friendly menu to each client."""
        if self.is_super_node:
            string_to_send = f"Welcome to super peer #{self.id}! What would you like to do?\r\n"
            msg1 = bytes(string_to_send, 'utf-8')
        else:
            string_to_send = f"Welcome to leaf node #{self.id}! What would you like to do?\r\n"
            msg1 = bytes(string_to_send, 'utf-8')

        while True:
            msg2 = b'----------------------------------------------------------\r\n'
            msg3 = b'1: Search for a file and download it\r\n'
            msg4 = b'2: See my list of files\r\n'
            msg7 = b'3: Delete a file from this node/peer\r\n'
            msg5 = b'\r\n'
            msg6 = b'Choice: '
            self.client.send(msg1 + msg2 + msg3 + msg4 + msg7 + msg2 + msg6)
            choice_string = self.client.recv(8)
            try:
                choice = int(choice_string)
                if choice > 3:
                    self.client.send(msg5)
                    self.client.send(
                        b'Please enter in a valid menu choice!\r\n')
                    self.client.send(msg5)
                    continue
            except ValueError:
                self.client.send(msg5)
                self.client.send(b'Please enter in a valid integer.\r\n')
                self.client.send(msg5)
                continue

            self.client.send(msg5)
            return choice

    def display(self):
        """Helper method that displays each file and the nodes it belongs to"""
        if self.is_super_node:
            for file in self.files:
                nodes = self.files[file]
                string_to_send = f"File: {file}, belongs to Nodes: {nodes}\r\n"
                msg = bytes(string_to_send, 'utf-8')
                self.client.send(msg)
        else:
            self.client.send(b'These are the files I own:\r\n')
            for file in self.files:
                string_to_send = f"{file}\r\n"
                msg = bytes(string_to_send, 'utf-8')
                self.client.send(msg)
            string_to_send = f"\r\nThese are files I downloaded from other leaf-nodes:\r\n"
            msg = bytes(string_to_send, 'utf-8')
            self.client.send(msg)
            if not self.downloads:
                self.client.send(b'No downloads yet!\r\n')
            else:
                for file in self.downloads:
                    string_to_send = f"{file}\r\n"
                    msg = bytes(string_to_send, 'utf-8')
                    self.client.send(msg)
        
        self.client.send(b'\r\n')

    def search_for_file(self):
        """Helper method that displays each file and the nodes it belongs to"""
        while True:
            self.client.send(b'Search for a file: ')
            file_name = self.client.recv(1024)
            file = file_name.decode("utf-8", "ignore").rstrip()
            if file in self.files or file in self.downloads:
                self.client.send(b'\r\nYou have this file! Try another search.\r\n\r\n')
            else:
                return file

    def choose_port(self, arr):
        """Helper method that displays port numbers and asks the user to choose one"""
        arr = filter(None, arr)
        arr = [int(i) for i in arr]
        arr = sorted(i for i in arr if i > 12345 and i < 12384)
        if not arr:
            return None
        while True:
            self.client.send(
                b'The file you searched for is available on these ports: \r\n')
            for port in arr:
                string_to_send = f"Port: {port}\r\n"
                msg = bytes(string_to_send, 'utf-8')
                self.client.send(msg)
            self.client.send(b'Which port do you want?\r\n')
            self.client.send(b'Choice: ')
            choice_string = self.client.recv(8)
            try:
                choice = int(choice_string)
                if choice in arr:
                    self.client.send(b'\r\n')
                    return choice
                self.client.send(b'\r\nPlease enter in a valid port.\r\n\r\n')
            except ValueError:
                self.client.send(b'\r\nPlease enter in a valid integer.\r\n\r\n')

    def remove(self):
        """Helper method that displays a node/super-peer's files and asks the user which one to delete"""
        while True:
            self.display()
            self.client.send(b'Which one would you like to delete?\r\n')
            self.client.send(b'Enter in \'q\' to go back to the menu.\r\n')
            self.client.send(b'Choice: ')
            choice_string = self.client.recv(16)
            choice_string = choice_string.decode("utf-8", "ignore").rstrip()
            if choice_string in self.downloads:
                self.client.send(b'\r\n')
                return choice_string
            elif choice_string in self.files:
                self.client.send(b'\r\nRemoving your master files is not supported.\r\n\r\n')
            elif choice_string == 'q':
                self.client.send(b'\r\n')
                return choice_string
            else:
                self.client.send(b'\r\nPlease enter in a valid file name.\r\n\r\n')